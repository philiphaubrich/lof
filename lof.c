/* Yet another report shindig
Gotta start somewhere
Copyright Philip Haubrich, 2014

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


TODO: Hmmm, something isn't exiting right. Sometimes it core-dumps Signal 80 when I close the program.

README:
1) Open the fieldNames.txt file and adjust the 8 headers and 4 fields to match the output of spectrometer. If you're unsure, create a log, open it, and inspect the text that it outputs. The names have to match exactly. 

2) Run this program in the folder that the spectrometer creates it's logfiles. It has to output one log per file, and it has to be a .csv file.

This program polls for csv files in it's local directory. It parses the data, saves the first 30 to a sql table, and then calculates the standard deviation. From there on, files that are read are binned according to how they fall with +-2SD of two different parameters. The user hits the done button at some point and the things makes a report on the average standard deviation and whatnot of the various bins.

And yeah, all that happens by default, but the user can adjust the acceptable M-distance, the calibration pool size, and the bin-sortin rulesets. 




CHANGELOG:
v3.5 Linux Build. 

v3.4 Abstracting fields. Now the user can define the fields he wants. But only 4. And there are still a lot of areas that are hardcoded around their location. Still, it's something.
It also picks up half-batches if you close half-way through. And should recognize and respect old recorded batches.
Probably bugs with this part of it though, needs some serious test.

v3.3 Cleaned up some of the language in the error reports. Got rid of the herpaderp field.

v3.2 Minor changes

v3.1 Added user-defined rulesets and got users out of DLL HELL

v3 GTK GUI

v2.2 Improved how it deals with moving locked files and name-conflicts

v2 sql db to keep track of calibration data

v1 all static text, simple math.

*/

#include "lof.h"
#include "gui.h"
#include "ruleSet.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h> 
#include "wsql.h"

//Looking at filesystems
#ifdef WINDOWS
#include <Windows.h>
#else
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>     //Access for fileExists
#endif


#define FLOATTHRESH         0.01f
#define BUFFSIZE    100000
#define ARRAYSIZE   50

//Yep. globals baby.
//This is mostly because we need to shuffle data back and forth with the GUI side of things. I thought about having an interface with field lookups and shit, but then I remembered how much I'm getting paid and appreciated for this. So globals it is. 
struct CAL_DATA CalData;
struct LOG_DATA LogData;
char headerNames[NUMHEADERS][TYPICALSIZE];
char fieldNames[NUMFIELDS][TYPICALSIZE];
int currentBatch=0;
int logCount=0;
int calFlag=0;  //Have we gotten 30 samples for the calibration?
int calPoolSize=30;
float mDistThreshold = 5.00f;
int lastScanValid = 1;
char errMsg[TYPICALSIZE];



/**wow, strtok is stupid. It throws away empty fields.
//I can't believe I'm making my own tokenizer
//get index of next deliminator after the previous index
//load string from previous index to next, append null
//repeat till null found.
//Yeah yeah, this is a lot more involved than it needs to be. But you know what? I like mucking about in C, and strtok doesn't do what I want it to do, and I've never actually done this before.
// ...and after all that I plop in into an array? And it eats comments.
*/
void myTokenize(char* line, size_t size, char array[ARRAYSIZE][TYPICALSIZE])
{
  //char tmp[100];
  char* pi;
  char* ni;
  int count=0;
  pi = line;
  ni = line;
  while(*ni != '\0' && ni < line+size)
  {
    while(*ni != ',' && *ni != '\0')
    {
      ni++;
    }
    int tmpSize = ni-pi;
    if(ni-pi > 100)
    tmpSize = 99;
    memcpy(array[count], pi, tmpSize);
    array[count][tmpSize] = '\0';
    
    //printf("token: %s\n",tmp);
    
    count++;
    ni++;
    pi = ni;
  }
  
  /*
  int i;
  for(i=0; i< ARRAYSIZE; i++)
  {
    printf("%s\n",array[i]);
  }
  */
}

///Yeah, I dunno man. usleep just doesn't seem to work in the windows environment
void mysleep(int sec)
{
  int start = time(NULL);
  while( time(NULL) - start < sec)  ; //spin baby

  //eh, apparently I should be using double difftime (time_t end, time_t beginning);  but meh
}


///helper func for printing to the screen and to the gui message box.
///Not yet Variadic cause I haven't needed it.
void dualPrintf(char* msg)
{
    printf("%s\n",msg);
    sprintf(errMsg,"%s",msg);
    updateInterfaceString(errMsg);
}


///Takes a tokenized array, and puts the data into a struct.
//TODO: The incoming data sometimes has a space between the conc and mdist, sometimes not. It's a problem.   So far we're just ignoring it and hoping it doesn't come back. But hell, that was back when I was working with Dan, so who knows what the fuck he was doing. The trustworthiness of his bug reports is sketchy at best
void parseLog(char array[ARRAYSIZE][TYPICALSIZE])
{
  int i, j;
  int mDistOffset = 2;
  
  //Clear out any data, in case something breaks
  memset(&LogData, 0, sizeof LogData);
  
  LogData.batch = currentBatch;
  
  for(i=0; i < ARRAYSIZE; i++)
  {
    for(j=0; j < NUMHEADERS; j++)
    {
      if( strcmp(array[i], headerNames[j]) == 0)
      {
        sprintf(LogData.headers[j], "%s", array[i+1]);
      }
    }
    
    for(j=0; j < NUMFIELDS; j++)
    {
      if( strcmp(array[i], fieldNames[j]) == 0)
      {
        sscanf(array[i+1],"%f",&(LogData.field_conc[j]));
        sscanf(array[i+mDistOffset],"%f",&(LogData.field_mDist[j]));
      }
    }
    
    //moisture is special. 
    if( strcmp(array[i], "Moisture") == 0)
    {
      sscanf(array[i+1],"%f",&(LogData.moisture_conc));
      sscanf(array[i+mDistOffset],"%f",&(LogData.moisture_mDist));
    }
  }
  
  return;
}

///Yeah, I dunno man, he just doesn't want to see negative numbers.
///This was one of the two things he asked for the program to do originally.
///Huh fixed a bug in 3.4 where it cleared out _conc when _mdist was low
void filterLog()
{
  int i;
  for(i=0; i < NUMFIELDS; i++)
  {
    if(LogData.field_conc[i] < FLOATTHRESH) LogData.field_conc[i] = 0.0f;
    if(LogData.field_mDist[i] < FLOATTHRESH) LogData.field_mDist[i] = 0.0f;
  }

  if(LogData.moisture_conc < FLOATTHRESH) LogData.moisture_conc = 0.0f;
  if(LogData.moisture_mDist < FLOATTHRESH) LogData.moisture_mDist = 0.0f;
}


///Dry basis of X = X predicted value/((100-moisture)/100)
void calcDryBasis()
{
  int i;
  float div = ((100.0f-LogData.moisture_conc)/100.0f);
  if(div == 0.0f)
  {
    printf("div zero error for dry basis\n");
    exit(-1);
  }

  for(i=0; i < NUMFIELDS; i++)
  {
    LogData.field_dryBasis[i] =  LogData.field_conc[i]/div;
  }

}

  
void printLog()
{
  int i;
  printf("Batch: %d\n",LogData.batch);
  
  for(i=0; i < NUMHEADERS; i++)
  {
    printf("%s %s\n", headerNames[i], LogData.headers[i]);
  }
  
  for(i=0; i < NUMFIELDS; i++)
  {
    printf("%s_conc: %f\n",     fieldNames[i], LogData.field_conc[i]);
    printf("%s_mDist: %f\n",    fieldNames[i], LogData.field_mDist[i]);
    printf("%s_dryBasis: %f\n", fieldNames[i], LogData.field_dryBasis[i]);
  }

  printf("moisture_conc: %f\n", LogData.moisture_conc);
  printf("moisture_mDist: %f\n", LogData.moisture_mDist);
  
  printf("bin: %d\n", LogData.bin);
  
  printf("\n\n");
}



//File System Fun between windows and Linux.   TODO, think about moving these functions out to their own files and leaving a HW abstraction laywer. Leave the linking to the makefile. 
#ifdef WINDOWS
BOOL DirectoryExists(LPCTSTR szPath)
{
  oWORD dwAttrib = GetFileAttributes(szPath);
linux:

  return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
         (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

int MoveFileShim(char* filename, char* moveName)
{
  return MoveFile(filename, moveName);
}

/// Kinda wierd, but this is how MS suggests we look to see if a file exists. 
int fileExists(char* filename)
{
  WIN32_FIND_DATA ffd;
  HANDLE hFind = INVALID_HANDLE_VALUE;
  int found;
  hFind = FindFirstFile(filename, &ffd);
  found = (hFind != INVALID_HANDLE_VALUE);
  if(found)
  {
    FindClose(hFind);
  }
  return found;
}


#else
int DirectoryExists(char* path)
{
  int ret=0;
  DIR* dir = opendir(path);
  if(dir)
  {
    closedir(dir);
    ret = 1;
  }
  return ret;
}

int MoveFileShim(char* filename, char* moveName)
{
  return rename(filename, moveName);
}

int fileExists(char* filename)
{
  return (access(filename, F_OK) != -1);
}


#endif


///The flag is a bit of shenanigians. We need to reject logs that are OOR before cal, after cal, we still process them and trust the ruleSet to catch them. 
int isValid(int calFlag)
{
  if(calFlag != 0)
    return 1;
  if(LogData.moisture_mDist > mDistThreshold ||
     LogData.field_mDist[1] > mDistThreshold ||
     LogData.field_mDist[2] > mDistThreshold )
  {
    //TODO ugh.... we need to have the user decide if any given field is "critical"..... 
    dualPrintf("That one wasn't valid. M-Distance for a critical value was too high");
    return 0;
  }
  return 1;
}



/**After the file has been parsed and logged, move it to a folder for safekeeping/historical logs.
Some issues with the file not being released by the spectrometer that generates the file before this tries to move it.
That's it. This function is GOING to work.
*/
void moveLog(FILE* f_log, char* filename, int calFlag)
{
  char folder[TYPICALSIZE];
  char moveName[TYPICALSIZE]; 
  int i;
  
  if(calFlag)
    sprintf(folder,"logged");
  else
    sprintf(folder,"tmp");
    
  if(!DirectoryExists(folder))
  {
#ifdef WINDOWS
    mkdir(folder);
#else
    mkdir(folder, 0777);
#endif
  }
  fclose(f_log);

  sprintf(moveName,"%s/%s",folder,filename);
  if(!MoveFileShim(filename,moveName))
  {
    printf("Can't move files [%s]->[%s] Let's try a different name.\n",filename,moveName);
    sprintf(moveName,"%s/%c%s",folder,"abcdefghijklmnopqrstuvwxyz"[rand()%26],filename); //Yeah, you read that right, it tacks on a random letter. 
    if(!MoveFileShim(filename,moveName))
    {
      printf("Still couldn't move the file [%s]->[%s]. \nLet's try just removing it\n",filename,moveName);
      
      if(remove(filename) != 0)
      {
        printf("It's locked. \nLet's wait a bit and try again.\n");
        for(i=0; i<10; i++)
        {    
          mysleep(5);
          printf("try %d of 10\n",i);
          if(remove(filename) == 0)
          {
            printf("It worked\n");
            return;
          }
        }
        printf("Can't move the file. Exiting.\n");
        exit(-1);        
      }
      printf("Removed file. Permanent record lost. Continuing\n");
      return;
    }
    printf("Name adjusted. Continuing\n");
    return;
  }
  //Normal operation, don't give a snarky pass message
}




///You change a value code-side, you need to go update the GUI as well.
///This whole setup is a bit crufty
void updateLastScanGUI()
{
  int i;
  updateInterfaceInt(&LogData.batch);
  
  for(i=0; i < NUMHEADERS; i++)
  {
    updateInterfaceString(LogData.headers[i]);
  }
  
  for(i=0; i < NUMFIELDS; i++)
  {
    updateInterfaceFloat(&LogData.field_conc[i]);
    updateInterfaceFloat(&LogData.field_mDist[i]);
    updateInterfaceFloat(&LogData.field_dryBasis[i]);
  }

  updateInterfaceFloat(&LogData.moisture_conc);
  updateInterfaceFloat(&LogData.moisture_mDist);
  updateInterfaceInt(&LogData.bin);
}



////Bloody stdio leaves the newline character
void stripNewLine(char* str, int size)
{
  int i=0; 
  for(i=0; i<size && str[i] != 0; i++)
  {
    if(str[i] == '\n' || str[i] == '\r')
    {
      str[i] = 0;
      return;
    }
  }
}

/**Loads fieldNames.txt,  user-defined stuff
Is one of the first things run, before even the gui stuff
Kind of a lame way of parsing a file...
*/
void getFieldNames()
{
  int i;
  FILE* f_fieldNames;
  
  f_fieldNames = fopen("fieldNames.txt", "r");
  printf("opening: [fieldNames.txt]\n");
  
  if(f_fieldNames == NULL)
  {
    printf("file io err, you need a file called \"fieldNames.txt\" with 8 headers and 4 fields\n");
    exit(-1);
  }
  
  for(i=0; i<NUMHEADERS; i++)
  {
    fgets(headerNames[i], 100, f_fieldNames);
    stripNewLine(headerNames[i], TYPICALSIZE);
    
    if(headerNames[i][0] == '#')
    {
      i--;    //ignore comments
      continue;
    } 
  }
  
  for(i=0; i<NUMFIELDS; i++)
  {
    fgets(fieldNames[i], 100, f_fieldNames);
    stripNewLine(fieldNames[i], TYPICALSIZE);
    
    if(fieldNames[i][0] == '#')
    {
      i--;    //ignore comments
      continue;
    } 
  }
}


/**Used to be main() in v1 and v2.  GTK took center stage and now this can't halt
Find log files, process them, stuff the important bits into sql, update the gui
*/
int lof()
{
  char logFilename[TYPICALSIZE]; 
  FILE* f_log;
  FILE* f_report;
  char line[BUFFSIZE];
  char array[ARRAYSIZE][TYPICALSIZE];
  size_t logSize;


  //In the currrent working folder, loop through all .csv files
  

  //Oh man, I really don't like how this is split. It really calls for a HAL.
#ifdef WINDOWS
  //Sigh, this is so much bloody easier than dealing with .NET bullshit.
  // Seriously, slap this in a while loop and call it done.   Now, detecting when a file is made is nice and all, I can certainly see where you'd NEED that. But I don't. So fuck it.
  WIN32_FIND_DATA ffd;
  HANDLE hFind = INVALID_HANDLE_VALUE;
  
  // Find the first file in the directory.
  hFind = FindFirstFile("*.csv", &ffd);

  if (INVALID_HANDLE_VALUE == hFind) 
  {
    //printf("Zero files to load\n");
    return 1;
  } 
  do
  {
    sprintf(logFilename, "%s", ffd.cFileName);
   
#else
  DIR* fd;
  struct dirent* inFile;
  fd = opendir(".");
  if(fd == NULL)
  {
    return 1;
  }
  inFile = readdir(fd);
  do
  {
    char* str = strrchr(inFile->d_name, '.');
    if(str == NULL)
      continue;
    if(strcmp(str, ".csv") != 0)
      continue;

    sprintf(logFilename, "%s", inFile->d_name);
#endif
  
    //Delay a second in case the device is still dicking with the file. Dunno, there's a weird bug Dan is seeing and this might fix it.
    mysleep(1);
    
    lastScanValid = 1;  //If the mdist on x,y,z are too high, we shouldn't use them in calibration 
  
    f_log = fopen( logFilename, "r");
    printf("opening: [%s]\n", logFilename);

    
    if(f_log == NULL || f_report == NULL) 
    {
      printf("file io err\n");
      return 1;
    }
    
    //Should just be the one
    logSize = fread(line, 1, BUFFSIZE, f_log);

    //Splits the log into tokens. Makes it easier to parse
    myTokenize(line, logSize, array);

    //Take the items and put it into our data structure
    parseLog(array);
    
    //He doesn't want any of the measurements to be negative or negligible
    filterLog();
    
    calcDryBasis();
    
    //Bin it
    if(calFlag) binSort();
    
    //Output the damn thing in some shape or form. Probably need something with a print button. 
    printLog();
    
    //Check to see if it's a valid sample for Calibration
    lastScanValid = isValid(calFlag);
    updateInterfaceInt(&lastScanValid);
  
    //Shove it all in sqlite
    if(lastScanValid) insertLog(calFlag);
    
    //move log to it's completed directory
    //Check if no directory, make directory, save file to directory, remove file from here. 
    //Also closes f_log
    moveLog(f_log, logFilename, calFlag);
    
    updateLastScanGUI();
    
    if(lastScanValid) 
    {
      logCount++;
      updateInterfaceInt(&logCount);
    }
    printf("\nCount: %d\n\n",logCount);
    if(logCount >= calPoolSize  && calFlag == 0)
    {
      //In case the user sets the poolsize low after a lot of samples. 
      calPoolSize = logCount;
      updateInterfaceInt(&calPoolSize);
      
      dualPrintf("Count >= poolsize, it's calibration time");
      calFlag = 1;
      updateInterfaceInt(&calFlag);
      calcCalibration(currentBatch); 
      logCount = 0;
      updateInterfaceInt(&logCount);
      
      //Save that calibration data to file.
      saveCalData();
    }
  }
#ifdef WINDOWS
  while (FindNextFile(hFind, &ffd) != 0);
  FindClose(hFind);
#else
  while ((inFile = readdir(fd)));
#endif
  
  return 1;
}



