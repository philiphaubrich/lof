This program is made to consume the log files created by a Malvern Panalytical ASD-range (Visible, NIR, SWIR) spectrometer. Runs alongside a program called Indico that operates the spectrometer.
It was made for free for a business that never got off the ground. 

This program polls for csv files in it's local directory. It parses the data, saves the first 30 to a sql table, and then calculates the standard deviation. From there on, files that are read are binned according to how they fall with +-2SD of two different parameters. The user hits the done button at some point and the things makes a report on the average standard deviation and whatnot of the various bins.

And yeah, all that happens by default, but the user can adjust the acceptable M-distance, the calibration pool size, and the bin-sortin rulesets. 


1) Open the fieldNames.txt file and adjust the 8 headers and 4 fields to match the output of spectrometer. If you're unsure, create a log, open it, and inspect the text that it outputs. The names have to match exactly. 

2) Run this program in the folder that the spectrometer creates it's logfiles. It has to output one log per file, and it has to be a .csv file.
