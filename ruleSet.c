/* RuleSet
Copyright Philip Haubrich, 2014

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "ruleSet.h"
#include "lof.h"
#include "gui.h"

#include <stdlib.h>

#define GREATERTHAN 1
#define LESSTHAN    2
#define EQUALTO     3
#define NOTEQUALTO  4




struct RULE_SET RuleSet[NUMRULES];

//So, the gui dropdown box needs an index. We want to use a floatPtr. This ties the two together and is a massive hackjob.
float* FieldLookup[NUMDROP] =
{
  NULL,
  &LogData.field_conc[0],  
  &LogData.field_mDist[0],
  &LogData.field_dryBasis[0],
  &LogData.field_conc[1],
  &LogData.field_mDist[1],
  &LogData.field_dryBasis[1],
  &LogData.field_conc[2],
  &LogData.field_mDist[2],
  &LogData.field_dryBasis[2],
  &LogData.field_conc[3],
  &LogData.field_mDist[3],
  &LogData.field_dryBasis[3],
  &LogData.moisture_conc,
  &LogData.moisture_mDist,
  &CalData.field_dryBasis_sd[0],
  &CalData.field_dryBasis_avg[0],
  &CalData.field_dryBasis_sd[1],
  &CalData.field_dryBasis_avg[1],
  &CalData.field_dryBasis_sd[2],
  &CalData.field_dryBasis_avg[2],
  &CalData.field_dryBasis_sd[3],
  &CalData.field_dryBasis_avg[3]
};

int lookupField(float* fltPtr)
{
  int i;
  for(i=0; i<NUMDROP; i++)
  {
    if(fltPtr == FieldLookup[i])
      return i;
  }
}

/*Create the default ruleset based on the SD of the first two constituents.
bin 1 invalids: high mDist: moisture, field 1&2  (and maybe negative values, but he never got back to me)
bin 2 low field#1
bin 3 high field#1
bin 4 low field#2
bin 5 high field#2
bin 6 everything else, ie, good-zone for both 

hi/lo is the average +/- 2 standard deviation
*/
void initRuleSet()
{
  RuleSet[0].targetBin = 1;
  RuleSet[0].field = &LogData.moisture_mDist;
  RuleSet[0].comparator = GREATERTHAN;
  RuleSet[0].value = mDistThreshold;
  
  RuleSet[1].targetBin = 1;
  RuleSet[1].field = &LogData.field_mDist[1];
  RuleSet[1].comparator = GREATERTHAN;
  RuleSet[1].value = mDistThreshold;
  
  RuleSet[2].targetBin = 1;
  RuleSet[2].field = &LogData.field_mDist[2];
  RuleSet[2].comparator = GREATERTHAN;
  RuleSet[2].value = mDistThreshold;
  
  RuleSet[3].targetBin = 2;
  RuleSet[3].field = &LogData.field_dryBasis[1];
  RuleSet[3].comparator = LESSTHAN;
  RuleSet[3].value = CalData.field_dryBasis_avg[1] - (2 * CalData.field_dryBasis_sd[1]);
   
  RuleSet[4].targetBin = 3;
  RuleSet[4].field = &LogData.field_dryBasis[1];
  RuleSet[4].comparator = GREATERTHAN;
  RuleSet[4].value = CalData.field_dryBasis_avg[1] + (2 * CalData.field_dryBasis_sd[1]);
  
  RuleSet[5].targetBin = 4;
  RuleSet[5].field = &LogData.field_dryBasis[2];
  RuleSet[5].comparator = LESSTHAN;
  RuleSet[5].value = CalData.field_dryBasis_avg[2] - (2 * CalData.field_dryBasis_sd[2]);
   
  RuleSet[6].targetBin = 5;
  RuleSet[6].field = &LogData.field_dryBasis[2];
  RuleSet[6].comparator = GREATERTHAN;
  RuleSet[6].value = CalData.field_dryBasis_avg[2] + (2 * CalData.field_dryBasis_sd[2]);
  
  RuleSet[7].targetBin = 6;   //ie, everything else. 
  RuleSet[7].field = &LogData.moisture_conc;
  RuleSet[7].comparator = NOTEQUALTO;
  RuleSet[7].value = 0;
  
  //Then go tell the gui to update all those fields.
  int i;
  for(i=0; i<8; i++)
  {
    updateInterfaceInt(&RuleSet[i].targetBin);   //FYI, the intvalue has to match the dropdown box's index.
    updateInterfaceFloatPtr(&RuleSet[i].field);  
    //Oh GOD, WTF did I just do. Did I type that? Is that the address of array's struct's float pointer being sent to a function so it can find the float pointer it needs to update with the correct pointer to an actual fucking variable. That's it, where's my cyanide capsule. I'm out. 
    updateInterfaceInt(&RuleSet[i].comparator);
    updateInterfaceFloat(&RuleSet[i].value); 
  }
  
}

//Sort based on the ruleset.
void binSort()
{ 
  int rule=0;
  
  for(rule=0; rule<NUMRULES; rule++)
  {
    //Check for bad rules
    if(RuleSet[rule].targetBin == 0 ||
       RuleSet[rule].targetBin > NUMBINS ||
       RuleSet[rule].field == NULL ||
       RuleSet[rule].comparator == 0 ||
       RuleSet[rule].comparator > NOTEQUALTO)
      continue;
    switch(RuleSet[rule].comparator)  //Can't figure out how to generalize comparators...
    {
      case GREATERTHAN:
        if(*RuleSet[rule].field > RuleSet[rule].value)
        {
          LogData.bin = RuleSet[rule].targetBin;
          return;
        }
      break;
      case LESSTHAN:
        if(*RuleSet[rule].field < RuleSet[rule].value)
        {
          LogData.bin = RuleSet[rule].targetBin;
          return;
        }
      break;
      case EQUALTO:
        if(*RuleSet[rule].field == RuleSet[rule].value)
        {
          LogData.bin = RuleSet[rule].targetBin;
          return;
        }
      break;
      case NOTEQUALTO:
        if(*RuleSet[rule].field != RuleSet[rule].value)
        {
          LogData.bin = RuleSet[rule].targetBin;
          return;
        }
      break;
      default:
        dualPrintf("OOB comparator in the ruleset.");
    }
  }
}



void clearRuleSet()
{
  int i;
  for(i=0; i < NUMRULES; i++)
  {
    RuleSet[i].targetBin = 0;
    RuleSet[i].field = NULL;
    RuleSet[i].comparator = 0;
    RuleSet[i].value = 0;
    
    updateInterfaceInt(&RuleSet[i].targetBin);
    updateInterfaceFloatPtr(&RuleSet[i].field);
    updateInterfaceInt(&RuleSet[i].comparator);
    updateInterfaceFloat(&RuleSet[i].value);
  }
}
