/* For Love of Family v3
Copyright Philip Haubrich, 2014

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


graphics suck!



Ok, so getting and setting data between widgets in the gui and data in lof. 
Everything in lof that matters is a global cause it's easy like that. shut up.
When the GUI constructs it's widgets it stores said widget and the associated global in an array.   Well, pointers thereof.
When the lof wants to update a value, like when it parses a file, it looks through the array for the ptr to the data that's being updated and updates the associated widget. 
When the GUI wants to update a lof value, it uses it's callback function to go set it. 
I dunno why this had to be this complex. 

It PAINS me to admit that .NET does a cleaner job of this. But maybe I just need to see how other people have done it. Use one of their frameworks.

*/



#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
//#include <gdk/gdk.h>


#include "gui.h"
#include "lof.h"
#include "wsql.h"
#include "ruleSet.h"



#define VERSION "ForLoveOfFamily_v3.4"

#define COPYRIGHT "COPYRIGHT Philip Haubrich, 2014, released under GPLv3"

//mostly here to help tie the collection of data item manipulation between the gui and the lof program

struct WIDGET_INTERFACE 
{
  GtkWidget *widget;
  int widgetType;  //It's going to be GTK_ENTRY or GTK_COMBO_BOX
  char* strPtr;
  int* intPtr;
  float* floatPtr;
  float** floatPtrPtr;
  int comboboxIndex;
};

#define INTERFACE_SIZE 100
struct WIDGET_INTERFACE Interface[INTERFACE_SIZE];
int g_interfaceCount=0;


/**Ok, so the lof needs to update the WIDGETS in the gui. 
It assumes there's this Interface array built up with paired widgets and pointers to it's global variables. 
When the lof changes a variable on it's side, lof pokes this function to go update the gui and widget.
Perhaps it's not the most elegant solution, but I'm all outta fucks. (And the amount of times I've been saying that is too damn high)
Ugh, this doesn't scale well at all. it has a giant table of pointers, it has to think about EACH TYPE. 
*/
void updateInterfaceInt(int* ptr)
{
  int i;
  gchar str[100];
  
  //Could use a hashtable if I cared about speed. 
  for(i=0; i<g_interfaceCount && i < INTERFACE_SIZE; i++)
  {
    if(Interface[i].intPtr == ptr)
    {
      switch(Interface[i].widgetType)
      {
        case 0: //GTK_ENTRY:  
          sprintf(str,"%d",*(Interface[i].intPtr));
          gtk_entry_set_text(GTK_ENTRY(Interface[i].widget),str); 
          break;
        case 1: //GTK_COMBO_BOX:
          gtk_combo_box_set_active(GTK_COMBO_BOX(Interface[i].widget), *(Interface[i].intPtr));
          break;
        case 2: //GTK_BUTTON: It's just the reject button
          if(*(Interface[i].intPtr))
          {
            gtk_button_set_label(GTK_BUTTON(Interface[i].widget),"Reject");
          }
          else
          {
            gtk_button_set_label(GTK_BUTTON(Interface[i].widget),"!Rejected!");
          }
          break;
        default:
          dualPrintf("Issue with interface");
          printf("wtf is a widget of type:%d\n?",Interface[i].widgetType);
      }
      return;
    }
  }
}

///Same as the one above. This one is simpler because all floats are stored in a GTK_ENTRY
void updateInterfaceFloat(float* ptr)
{
  int i;
  //Could use a hashtable if I cared about speed. 
  for(i=0; i<g_interfaceCount; i++)
  {
    if(Interface[i].floatPtr == ptr)
    {
      gchar str[100];
      sprintf(str,"%f",*(Interface[i].floatPtr));
      gtk_entry_set_text(GTK_ENTRY(Interface[i].widget),str); 
      return;
    }
    
  }
}

///Yeah, so this is used in the RuleSet shenanigians. The field it uses is itself a pointer, but the comobox update needs an index. Which makes this a bit awkward. 
void updateInterfaceFloatPtr(float** ptr)
{
  int i;
  //Could use a hashtable if I cared about speed. 
  for(i=0; i<g_interfaceCount; i++)
  {
    if(Interface[i].floatPtrPtr == ptr)
    {
      //Yeah, we assume it's a combobox. But we could do the whole switch thing if we really wanted to.
      gtk_combo_box_set_active(GTK_COMBO_BOX(Interface[i].widget), lookupField(*Interface[i].floatPtrPtr));
    }
  }
}

///Same as above
void updateInterfaceString(char* ptr)
{
  int i;
  //Could use a hashtable if I cared about speed. 
  for(i=0; i<g_interfaceCount; i++)
  {
    if(Interface[i].strPtr == ptr)
    {
      gtk_entry_set_text(GTK_ENTRY(Interface[i].widget),Interface[i].strPtr); 
      return;
    }
  }
}








///Callback for the window's exit button
static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    gtk_main_quit();
    return FALSE;
}




/*
So when the GUI wants to change something in the lof, it has to have these awkward individual callback functions. I mean, I guess it's not really any worse than that fiasco with the INTERFACE. 
*/

///End batch button. All sorts of stuff happens here.  Primary output, clear everything, and reset
void CB_endBatch(GtkWidget *widget, gpointer data)
{
  int i;
  
  if(logCount == 0)
  {
    dualPrintf("No, batch count is zero");
    return;
  }

  //populate GUI with... something
  makeReport();
  
  currentBatch++;
  updateInterfaceInt(&currentBatch);
  
  logCount = 0;
  calFlag = 0;
  updateInterfaceInt(&logCount);
  updateInterfaceInt(&calFlag);
  
  for(i=0; i < NUMFIELDS; i++)
  {
    CalData.field_dryBasis_sd[i] = 0;
    CalData.field_dryBasis_avg[i] = 0;
    updateInterfaceFloat(&CalData.field_dryBasis_sd[i]);
    updateInterfaceFloat(&CalData.field_dryBasis_avg[i]);
  }
  CalData.count = 0;
  
  clearRuleSet();
  
  clearCalData();
}

///User is manually changing the batch number for some reason.
void CB_batchChange(GtkWidget *widget, gpointer data)
{
  //TODO validate, reject bad input, overwrite textbox with previous value
  sscanf(gtk_entry_get_text(GTK_ENTRY(widget)),"%d", &currentBatch);
}

///User is manually changing the poolsize, totally legit.
void CB_calibrationPoolSizeChange(GtkWidget *widget, gpointer data)
{
  //TODO validate, reject bad input, overwrite textbox with previous value
  sscanf(gtk_entry_get_text(GTK_ENTRY(widget)),"%d", &calPoolSize);
}

///User manually changing the mDist threshold, probably higher, probably unethical as anything above 5 is "supposed" to be untrustworthy. 
void CB_mDistThresholdChange(GtkWidget *widget, gpointer data)
{
  //TODO validate, reject bad input, overwrite textbox with previous value
  sscanf(gtk_entry_get_text(GTK_ENTRY(widget)),"%f", &mDistThreshold);
}

///All these callbacks are for setting the user-controlled rulesets. This was the big fat turd that Dan dropped on me during the second meeting. Now, he has a default ruleset, but just in case he needs to fudge everything, he wants to be able to change the sorting rules on the fly. 
void CB_rulesetBin(GtkWidget *widget, gpointer data)
{
  int opt, i;
  opt = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
  i=(int)data;
  //Validate the rulset index
  if(i<0 || i>NUMRULES) return;
  
  //if I REALLY cared, I could develop an interface between ruleSet.c and use a setter... but meh.
  RuleSet[i].targetBin = opt;
}

void CB_rulesetComparator(GtkWidget *widget, gpointer data)
{
  int opt, i;
  opt = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
  i=(int)data;
  //Validate the rulset index
  if(i<0 || i>NUMRULES) return;
  
  RuleSet[i].comparator = opt;
}

void CB_rulesetValue(GtkWidget *widget, gpointer data)
{
  int i;
  i=(int)data;
  //Validate the rulset index
  if(i<0 || i>NUMRULES) return;
  sscanf(gtk_entry_get_text(GTK_ENTRY(widget)),"%f", &(RuleSet[i].value));
}

void CB_rulesetField(GtkWidget *widget, gpointer data)
{
  int opt, i;
  opt = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
  i=(int)data;
  //Validate the rulset index
  if(i<0 || i>NUMRULES) return;
  
  RuleSet[i].field = FieldLookup[opt];
}


///This was feature creep. A button to reject bad samples. I dunno what the use-case is here, because anything that has a high M-Dist will already be rejected. But maybe the user scans a thing twice or something.
void CB_reject(GtkWidget *widget, gpointer data)
{
  if(logCount == 0)
  {
    dualPrintf("No, Batch count is zero");
    //TODO I could add a timer or something here so it clears out the error after a while
    return;
  }

  if(lastScanValid)
  {
    lastScanValid = 0;
    lof_reject();
    logCount--;
    updateInterfaceInt(&logCount);
  }
  else
  {
    gtk_button_set_label(GTK_BUTTON(widget),"Yeah, man, it's already rejected");
  }
}




//These functions below are to help cut down on the sheer CRUFT that GTK GUI's create. 

///Header crap from the last scan.
void postHeaderCrap(const char* fieldName, char* ptr, int entry, GtkWidget *parent)
{
  GtkWidget *widget;
  GtkWidget *box;
  
  box = gtk_hbox_new(FALSE, 0);
  {
    widget = gtk_label_new(fieldName);
    gtk_misc_set_alignment(GTK_MISC(widget),0.0,0.5);
    gtk_box_pack_start(GTK_BOX(box), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);

    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].strPtr = ptr;
    g_interfaceCount++;
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 30);
    gtk_box_pack_start(GTK_BOX(box), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);
  }
  gtk_box_pack_start(GTK_BOX(parent), box, FALSE, FALSE, 0);
  gtk_widget_show(box);
}

///The constituent fields of the last scan,   conc, mdist, and dryBasis. 
void lastScanTable(GtkWidget *parent)
{
  GtkWidget *widget;
  char str[100];

  int i;
  for(i=0; i < NUMFIELDS; i++)
  {  
    widget = gtk_label_new(fieldNames[i]);
    gtk_table_attach_defaults( GTK_TABLE(parent), widget, 0,1, 1+i,2+i);
    gtk_widget_show(widget);
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].floatPtr = &(LogData.field_conc[i]);
    g_interfaceCount++;
    gtk_entry_set_max_length(GTK_ENTRY(widget), 10);
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 10);
    gtk_table_attach_defaults( GTK_TABLE(parent), widget, 1,2, 1+i,2+i);
    gtk_widget_show(widget);
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].floatPtr = &(LogData.field_mDist[i]);  
    g_interfaceCount++;
    gtk_entry_set_max_length(GTK_ENTRY(widget), 10);
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 10);
    gtk_table_attach_defaults( GTK_TABLE(parent), widget, 2,3, 1+i,2+i);
    gtk_widget_show(widget);
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].floatPtr = &(LogData.field_dryBasis[i]);  
    g_interfaceCount++;
    gtk_entry_set_max_length(GTK_ENTRY(widget), 10);
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 10);
    gtk_table_attach_defaults( GTK_TABLE(parent), widget, 3,4, 1+i,2+i);
    gtk_widget_show(widget);
  }
}


///OH GOD! Constructing GUIs in text is a bloody nightmare.
int main( int argc, char *argv[]) 
{
  GtkWidget *window;
  GtkWidget *topVbox;
  GtkWidget *box1;  //heirarchy of boxes for packing
  GtkWidget *box2;
  GtkWidget *box3;
  GtkWidget *box4;
  GtkWidget *box5;
  GtkWidget *workerBox; //Or bottom box for packing things into
  GtkWidget *workerTable;
  GtkWidget *widget; //What we're currently stuffing into what
  char str[100];

  getFieldNames(); //Loads fieldNames.txt,  user-defined stuff
  
  gtk_init(&argc, &argv);
  
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  // always attach delete_event signal to the main window.
  g_signal_connect(window, "delete-event", G_CALLBACK(delete_event), NULL);
  gtk_signal_connect(GTK_OBJECT(window), "delete_event", GTK_SIGNAL_FUNC(gtk_exit), NULL);
  
  gtk_container_set_border_width(GTK_CONTAINER(window), 2);
  
  topVbox = gtk_vbox_new(FALSE, 0);
  
  
  //Top row reserved for name, version, logo, whatnot. 
	widget = gtk_label_new(VERSION);
	gtk_box_pack_start(GTK_BOX(topVbox), widget, FALSE, FALSE, 3);
	gtk_widget_show(widget);
  
  //Don't you even think of stripping this out.
  widget = gtk_label_new(COPYRIGHT);
	gtk_box_pack_start(GTK_BOX(topVbox), widget, FALSE, FALSE, 3);
	gtk_widget_show(widget);
  
  //Row with the batch# count# calibrated flag and number of cal points
  workerBox = gtk_hbox_new(FALSE, 0);
  {
    widget = gtk_label_new("Batch:");
    gtk_box_pack_start(GTK_BOX (workerBox), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;  //TODO: maybe. I'd like to use an actual GTK_TYPE like thing, but meh
    Interface[g_interfaceCount].intPtr = &currentBatch;
    g_interfaceCount++;
    gtk_entry_set_max_length(GTK_ENTRY(widget), 4);
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 4);
    gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_batchChange), (gpointer) "uhhh?");
    gtk_box_pack_start(GTK_BOX(workerBox), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);


    widget = gtk_label_new("Count:");
    gtk_box_pack_start(GTK_BOX(workerBox), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].intPtr = &logCount;
    g_interfaceCount++;
    gtk_entry_set_max_length(GTK_ENTRY(widget), 4);
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 4);
    gtk_box_pack_start(GTK_BOX(workerBox), widget, FALSE, FALSE, 3);
    gtk_widget_show(widget);    
  }
  gtk_box_pack_start(GTK_BOX(topVbox), workerBox, FALSE, FALSE, 0);
  gtk_widget_show(workerBox);
  
  //Main section with the interesting stuff: Last scan on left, sorting on right
  box1 = gtk_hbox_new(FALSE, 0);
  { 
    //Last scan
    box2 = gtk_vbox_new(FALSE, 0);
    {
      widget = gtk_label_new("Last Scan:");
      gtk_misc_set_alignment(GTK_MISC(widget),0.0,0.5);
      gtk_box_pack_start(GTK_BOX (box2), widget, FALSE, FALSE, 3);
      gtk_widget_show(widget);
     
      int i=0;
      for(i=0; i < NUMHEADERS; i++)
      {
        postHeaderCrap(headerNames[i], LogData.headers[i],  i, box2);
      }
      
      //Rows upon rows of data
      workerTable = gtk_table_new( 4, 5, FALSE);
      {
        widget = gtk_label_new(" Constituent Name ");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,1, 0,1);
        gtk_widget_show(widget);
        
        widget = gtk_label_new(" Concentration ");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 1,2, 0,1);
        gtk_widget_show(widget);
        
        widget = gtk_label_new(" M-Dist ");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 2,3, 0,1);
        gtk_widget_show(widget);
        
        widget = gtk_label_new(" Dry Basis ");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 3,4, 0,1);
        gtk_widget_show(widget);
      
        //Adds all those entry boxes.
        lastScanTable(workerTable);
      }
      gtk_box_pack_start(GTK_BOX(box2), workerTable, FALSE, FALSE, 0);
      gtk_widget_show(workerTable);
     
    }
    gtk_box_pack_start(GTK_BOX(box1), box2, FALSE, FALSE, 0);
    gtk_widget_show(box2);
    
    //Sorting on the right side
    workerTable = gtk_table_new( 8, 3, FALSE);
    {
      widget = gtk_label_new(" SORTING ");
      gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,3, 0,1);
      gtk_widget_show(widget);
      
      widget = gtk_label_new(" Target Bin ");
      gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,1, 1,2);
      gtk_widget_show(widget);
      
      widget = gtk_label_new(" Constituent ");
      gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 1,2, 1,2);
      gtk_widget_show(widget);
      
      widget = gtk_label_new(" Comparator ");
      gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 2,3, 1,2);
      gtk_widget_show(widget);
      
      widget = gtk_label_new(" Value ");
      gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 3,4, 1,2);
      gtk_widget_show(widget);
      
      int numRuleset = 10;
      int i;
      for(i = 0; i < numRuleset; i++)
      {
        widget = gtk_combo_box_new_text();
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), " ");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #1");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #2");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #3");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #4");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #5");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #6");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #7");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #8");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "Bin #9");
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 1;
        Interface[g_interfaceCount].intPtr = &(RuleSet[i].targetBin);
        g_interfaceCount++;
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_rulesetBin), (gpointer) i);
        gtk_table_attach_defaults(GTK_TABLE(workerTable), widget,0,1, 2+i,3+i);
        gtk_widget_show(widget);
        

        widget = gtk_combo_box_new_text();
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), " ");
        
        //Moisture is just special
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "moisture_conc");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "moisture_mDist");
        
        int j;
        char name[TYPICALSIZE];
        for(j=0; j < NUMFIELDS; j++)
        { 
          sprintf(name,"%s_conc", fieldNames[j]); 
          gtk_combo_box_append_text(GTK_COMBO_BOX(widget), name);
          sprintf(name,"%s_mDist", fieldNames[j]);
          gtk_combo_box_append_text(GTK_COMBO_BOX(widget), name);
          sprintf(name,"%s_dryBasis", fieldNames[j]);
          gtk_combo_box_append_text(GTK_COMBO_BOX(widget), name);
        }
        
        for(j=0; j < NUMFIELDS; j++)
        { 
          sprintf(name,"%s_dryBasis_sd", fieldNames[j]); 
          gtk_combo_box_append_text(GTK_COMBO_BOX(widget), name);
          sprintf(name,"%s_dryBasis_avg", fieldNames[j]);
          gtk_combo_box_append_text(GTK_COMBO_BOX(widget), name);
        }
        
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 1;
        Interface[g_interfaceCount].floatPtrPtr = &(RuleSet[i].field); 
        g_interfaceCount++;
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_rulesetField), (gpointer) i);
        gtk_table_attach_defaults(GTK_TABLE(workerTable), widget,1,2, 2+i,3+i);
        gtk_widget_show(widget);
        

        widget = gtk_combo_box_new_text();
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), " ");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), ">");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "<");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "=");
        gtk_combo_box_append_text(GTK_COMBO_BOX(widget), "!=");
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 1;
        Interface[g_interfaceCount].intPtr = &(RuleSet[i].comparator);
        g_interfaceCount++;
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_rulesetComparator), (gpointer) i);
        gtk_table_attach_defaults(GTK_TABLE(workerTable), widget, 2,3, 2+i,3+i);
        gtk_widget_show(widget);
        
        
        widget = gtk_entry_new();
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 0;
        Interface[g_interfaceCount].floatPtr = &(RuleSet[i].value);
        g_interfaceCount++;
        gtk_entry_set_max_length(GTK_ENTRY(widget), 10);
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 10);
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_rulesetValue), (gpointer) i);
        gtk_table_attach_defaults(GTK_TABLE(workerTable), widget, 3,4, 2+i,3+i);
        gtk_widget_show(widget);
      }   
      
    }
    gtk_box_pack_start(GTK_BOX(box1), workerTable, FALSE, FALSE, 10);
    gtk_widget_show(workerTable);
  }
  gtk_box_pack_start(GTK_BOX(topVbox), box1, FALSE, FALSE, 10);
  gtk_widget_show(box1);
  
  
  //Bottom half: Calibration on the left, final report on the right.
  box1 = gtk_hbox_new(FALSE, 0);
  { 
    //Calibration
    box2 = gtk_vbox_new(FALSE, 0);
    {
    
      workerBox = gtk_hbox_new(FALSE, 0);
      {
        widget = gtk_label_new("CALIBRATION");
        gtk_box_pack_start(GTK_BOX (workerBox), widget, FALSE, FALSE, 3);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("Pool Size:");
        gtk_box_pack_start(GTK_BOX (workerBox), widget, FALSE, FALSE, 3);
        gtk_widget_show(widget);
        
        widget = gtk_entry_new();
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 0;
        Interface[g_interfaceCount].intPtr = &calPoolSize;
        g_interfaceCount++;
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_calibrationPoolSizeChange), (gpointer) "uhhh?");
        gtk_entry_set_max_length(GTK_ENTRY(widget), 4);
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 4);
        gtk_box_pack_start(GTK_BOX(workerBox), widget, FALSE, FALSE, 10);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("M-Distance Threshold:");
        gtk_box_pack_start(GTK_BOX (workerBox), widget, FALSE, FALSE, 3);
        gtk_widget_show(widget);
        
        widget = gtk_entry_new();
        Interface[g_interfaceCount].widget = widget;
        Interface[g_interfaceCount].widgetType = 0;
        Interface[g_interfaceCount].floatPtr = &mDistThreshold;
        g_interfaceCount++;
        gtk_signal_connect(GTK_OBJECT(widget), "changed", GTK_SIGNAL_FUNC(CB_mDistThresholdChange), (gpointer) "uhhh?");
        gtk_entry_set_max_length(GTK_ENTRY(widget), 4);
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 4);
        gtk_box_pack_start(GTK_BOX(workerBox), widget, FALSE, FALSE, 10);
        gtk_widget_show(widget);
      }
      gtk_box_pack_start(GTK_BOX(box2), workerBox, FALSE, FALSE, 10);
      gtk_widget_show(workerBox);
      
      workerTable = gtk_table_new( 5, 3, FALSE);
      {
        int i;
        char name[TYPICALSIZE];
        
        widget = gtk_label_new("Standard Deviation");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 1,2, 0,1);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("Average");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 2,3, 0,1);
        gtk_widget_show(widget);
        
        
        for(i=0; i < NUMFIELDS; i++)
        {
          sprintf(name, "%s_dryBasis:",fieldNames[i]);
          widget = gtk_label_new(name);
          gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,1, i+1,i+2);
          gtk_widget_show(widget);
        }
        
        for(i=0; i < NUMFIELDS; i++)
        {
          widget = gtk_entry_new();
          Interface[g_interfaceCount].widget = widget;
          Interface[g_interfaceCount].widgetType = 0;
          Interface[g_interfaceCount].floatPtr = &CalData.field_dryBasis_sd[i]; 
          g_interfaceCount++;
          gtk_entry_set_width_chars(GTK_ENTRY(widget), 6);
          gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 1,2, i+1,i+2);
          gtk_widget_show(widget);
          
          widget = gtk_entry_new();
          Interface[g_interfaceCount].widget = widget;
          Interface[g_interfaceCount].widgetType = 0;
          Interface[g_interfaceCount].floatPtr = &CalData.field_dryBasis_avg[i];
          g_interfaceCount++;
          gtk_entry_set_width_chars(GTK_ENTRY(widget), 6);
          gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 2,3, i+1,i+2);
          gtk_widget_show(widget);
        }
      }
      gtk_box_pack_start(GTK_BOX(box2), workerTable, FALSE, FALSE, 10);
      gtk_widget_show(workerTable);
      
    }
    gtk_box_pack_start(GTK_BOX(box1), box2, FALSE, FALSE, 10);
    gtk_widget_show(box2);
    
    /*  Fuck that noise, They get text reports for now.
    //Lower Right side, with the Batch Report. 
    box2 = gtk_vbox_new(FALSE, 0);
    {
      workerTable = gtk_table_new( 8, 5, FALSE);
      {
        widget = gtk_label_new("BATCH REPORT");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,3, 0,1);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("Count");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 3,4, 1,2);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("Average");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 4,5, 1,2);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("Standard Deviation");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 5,6, 1,2);
        gtk_widget_show(widget);
        
        //Now... This is hard. Because we don't know how many bins were used, I dunno how many to make. And we of course don't have anything to put here yet as a batch hasn't ended. We'll have to populate this area when the user hits that button, but for now this is largely just spaceholder... 
                
        widget = gtk_label_new("Bin #1:");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,1, 2,3);
        gtk_widget_show(widget);
        
        widget = gtk_entry_new();
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 6);
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 3,4, 2,3);
        gtk_widget_show(widget);
        
        widget = gtk_label_new("");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 1,2, 3,4);
        gtk_widget_show(widget);
        
        widget = gtk_entry_new();
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 6);
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 4,5, 3,4);
        gtk_widget_show(widget);

        widget = gtk_entry_new();
        gtk_entry_set_width_chars(GTK_ENTRY(widget), 6);
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 5,6, 3,4);
        gtk_widget_show(widget);    

        widget = gtk_label_new("And such stuff...");
        gtk_table_attach_defaults( GTK_TABLE(workerTable), widget, 0,4, 4,5);
        gtk_widget_show(widget);        
        
        
      }
      gtk_box_pack_start(GTK_BOX(box2), workerTable, FALSE, FALSE, 10);
      gtk_widget_show(workerTable);
    }
    
    gtk_box_pack_start(GTK_BOX(box1), box2, FALSE, FALSE, 10);
    gtk_widget_show(box2);
    */
  }
  gtk_box_pack_start(GTK_BOX(topVbox), box1, FALSE, FALSE, 10);
  gtk_widget_show(box1);
  
  //Bottom line with eventThing, Bin #, and End Batch Button  
  box1 = gtk_hbox_new(FALSE, 10);
  { 
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].strPtr = errMsg;   //yep, global. 
    g_interfaceCount++;
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 80);
    gtk_box_pack_start(GTK_BOX(box1), widget, FALSE, FALSE, 10);
    gtk_widget_show(widget);   
    
    widget = gtk_label_new("LAST BIN:");
    gtk_box_pack_start(GTK_BOX(box1), widget, FALSE, FALSE, 10);
    gtk_widget_show(widget); 
    
    widget = gtk_entry_new();
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 0;
    Interface[g_interfaceCount].intPtr = &LogData.bin;
    g_interfaceCount++;
    gtk_entry_set_width_chars(GTK_ENTRY(widget), 3);
    gtk_box_pack_start(GTK_BOX(box1), widget, FALSE, FALSE, 10);
    gtk_widget_show(widget);
    
    widget = gtk_button_new_with_label("REJECT that last one");
    Interface[g_interfaceCount].widget = widget;
    Interface[g_interfaceCount].widgetType = 2;
    Interface[g_interfaceCount].intPtr = &lastScanValid;
    g_interfaceCount++;    
    gtk_signal_connect(GTK_OBJECT(widget), "clicked", GTK_SIGNAL_FUNC(CB_reject), (gpointer)"junk");
    gtk_box_pack_start(GTK_BOX(box1), widget, FALSE, FALSE, 10);
    gtk_widget_show(widget);
    
    
    widget = gtk_button_new_with_label("END BATCH");
    gtk_signal_connect(GTK_OBJECT(widget), "clicked", GTK_SIGNAL_FUNC(CB_endBatch), (gpointer) "cool button");
    gtk_box_pack_start(GTK_BOX(box1), widget, FALSE, FALSE, 10);
    gtk_widget_show(widget);
    
  }
  gtk_box_pack_start(GTK_BOX(topVbox), box1, FALSE, FALSE, 10);
  gtk_widget_show(box1);
  
  
  //Add it all in
  gtk_container_add (GTK_CONTAINER (window), topVbox);
  
  gtk_widget_show (topVbox);
  gtk_widget_show (window);

  //This is a one-time function that establishes databases, and configuration
  wsql_init();
  
  //Calls the forLoveOfFamily program proper. The part that crunches the data
  g_timeout_add_seconds(1,lof,NULL);
                      
                      
  gtk_main();
  
  lof_exit();
  
  return 0;
}
