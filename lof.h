#ifndef _lof_H_
#define _lof_H_


#define TYPICALSIZE 100



#define NUMHEADERS 8
#define NUMFIELDS 4     //Not counting Moisture
#define NUMRULES 9
#define NUMBINS 9




//The header fields of strings are largely unimportant. Apparently.
/*
Each of the real data values have two incoming values from the device:
Concentration
M-Distance, Which is how unsure the spectrometer is of the concentration value. Lower is better.
There's also the dry basis, which is concentration in relation to the moisture concentration, which is just special.
*/
struct LOG_DATA
{
  int  batch;
  char headers[NUMHEADERS][TYPICALSIZE];
  float field_conc[NUMFIELDS];
  float field_mDist[NUMFIELDS];
  float field_dryBasis[NUMFIELDS];

  float moisture_conc;
  float moisture_mDist;
  float moisture_dryBasis; //This one doesn't exist. No. It doesn't. Why are you even looking at it. IT's not real. Look away. Shhhhh, no questions. 
  int bin;  //Or char bin[6][TYPICALSIZE]  if he wants to name the bins.
};


// What we use for calibration and sorting. 
// After the first 30 samples, we fill this datastructure. 
// Used to get the default sorting values
struct CAL_DATA
{
  float field_dryBasis_sd[NUMFIELDS];
  float field_dryBasis_avg[NUMFIELDS];
  float moisture_conc_avg;// Added later. This is just here so the report maker has a place to stick data. This is what I get for dual-purposing a data structure.
  int batch;  //This field here is only used when loading in saved calData
  int count;  //This is the number of samples that was actually used for these calibration values.
};


//Gotta call that lof main somehow
int lof();
void getFieldNames(); //Loads fieldNames.txt,  user-defined stuff
void dualPrintf(char* msg);


//Yeah yeah, extensive use of globals. It gets the job done.
extern int currentBatch;
extern struct LOG_DATA LogData;
extern struct CAL_DATA CalData;
extern char headerNames[NUMHEADERS][TYPICALSIZE];
extern char fieldNames[NUMFIELDS][TYPICALSIZE];
extern int logCount;
extern int calFlag;  //Have we gotten 30 samples for the calibration?
extern int calPoolSize;
extern float mDistThreshold;
extern int lastScanValid;
extern char errMsg[TYPICALSIZE];



#endif
