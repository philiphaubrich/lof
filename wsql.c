/* With SQL   Everything that touches the DB goes here.  
Because it actually takes time to compile.
Copyright Philip Haubrich, 2014

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "wsql.h"
#include "lof.h"
#include "gui.h"
#include "sqlite3.h"
#include <math.h> //Just for sqrt
#include <stdlib.h>
#include <stdio.h>


extern int logCount;

sqlite3 *db;



struct BOOT_DATA
{
  int largestBatch;
  int count;
};



///Yeah, so when the program runs the first time the database is empty and doesn't have any scheme.
///Wish this could have a better naming scheme, but he wants it all to be user-editable.  UUUUgggghhhhh
void makeTables()
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  
  sprintf(sqlCmd,"create table tblLog(\
batch int,\
header0 varchar(100),\
header1 varchar(100),\
header2 varchar(100),\
header3 varchar(100),\
header4 varchar(100),\
header5 varchar(100),\
header6 varchar(100),\
header7 varchar(100),\
field0_conc float,\
field0_mDist float,\
field0_dryBasis float,\
field1_conc float,\
field1_mDist float,\
field1_dryBasis float,\
field2_conc float,\
field2_mDist float,\
field2_dryBasis float,\
field3_conc float,\
field3_mDist float,\
field3_dryBasis float,\
moisture_conc float,\
moisture_mDist float,\
bin smallint);");

  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error making tables: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }     

  sprintf(sqlCmd,"create table tblTmpLog(\
batch int,\
header0 varchar(100),\
header1 varchar(100),\
header2 varchar(100),\
header3 varchar(100),\
header4 varchar(100),\
header5 varchar(100),\
header6 varchar(100),\
header7 varchar(100),\
field0_conc float,\
field0_mDist float,\
field0_dryBasis float,\
field1_conc float,\
field1_mDist float,\
field1_dryBasis float,\
field2_conc float,\
field2_mDist float,\
field2_dryBasis float,\
field3_conc float,\
field3_mDist float,\
field3_dryBasis float,\
moisture_conc float,\
moisture_mDist float,\
bin smallint);");

  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error making tables: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }    
  
    
  sprintf(sqlCmd,"create table tblCal(\
batch int,\
count int,\
field0_dryBasis_sd float,\
field0_dryBasis_avg float,\
field1_dryBasis_sd float,\
field1_dryBasis_avg float,\
field2_dryBasis_sd float,\
field2_dryBasis_avg float,\
field3_dryBasis_sd float,\
field3_dryBasis_avg float);");

  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error making tables: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }    
    
  return;
}


//SQLite uses callback functions to deal with data. 
//pArg points to a structure that'll be where I stick my end data
//nArg is # of columns in result
//azArg array of pointers to strings  with the data from  query
//azCol array of pointers to strings with the table names.

///Gets data for each bin in the final report
///    sprintf(sqlCmd,"select AVG(field0_dryBasis), AVG(field1_dryBasis), AVG(field2_dryBasis), AVG(field3_dryBasis), AVG(moisture_conc), count(*) from tblLog where bin=%d;",i);
static int callbackReport(void *pArg, int nArg, char **azArg, char **azCol)
{
  float tmp;
  struct CAL_DATA *p = (struct CAL_DATA *)pArg;
  int count;
  
  
  if(azArg[0] == NULL) //Empty table, no data.
  {
    p->count = 0;
    return 0;
  }
  sscanf(azArg[0],"%f",&tmp);
  p->field_dryBasis_avg[0] = tmp;
  sscanf(azArg[1],"%f",&tmp);
  p->field_dryBasis_avg[1] = tmp;
  sscanf(azArg[2],"%f",&tmp);
  p->field_dryBasis_avg[2] = tmp;
  sscanf(azArg[3],"%f",&tmp);
  p->field_dryBasis_avg[3] = tmp;
  
  sscanf(azArg[4],"%f",&tmp);
  p->moisture_conc_avg = tmp;
  
  sscanf(azArg[5],"%d",&count);
  p->count = count;

  return 0;
}


///At boot, grab the cal data,  if it exists.
///  sprintf(sqlCmd,"select * from tblCal;");
static int callbackCalData(void *pArg, int nArg, char **azArg, char **azCol)
{
  float tmp;
  struct CAL_DATA *p = (struct CAL_DATA *)pArg;
  int count;
  
  if(azArg[0] == NULL) //Empty table, no data.
  {
    p->batch = 0;
    p->count = 0;
    return 0;
  }
  
  sscanf(azArg[0],"%d",&count);
  p->batch = count;
  sscanf(azArg[1],"%d",&count);
  p->count = count;
  
  sscanf(azArg[2],"%f",&tmp);
  p->field_dryBasis_sd[0] = tmp;
  sscanf(azArg[3],"%f",&tmp);
  p->field_dryBasis_avg[0] = tmp;
  
  sscanf(azArg[4],"%f",&tmp);
  p->field_dryBasis_sd[1] = tmp;
  sscanf(azArg[5],"%f",&tmp);
  p->field_dryBasis_avg[1] = tmp;
  
  sscanf(azArg[6],"%f",&tmp);
  p->field_dryBasis_sd[2] = tmp;
  sscanf(azArg[7],"%f",&tmp);
  p->field_dryBasis_avg[2] = tmp;
  
  sscanf(azArg[8],"%f",&tmp);
  p->field_dryBasis_sd[3] = tmp;
  sscanf(azArg[9],"%f",&tmp);
  p->field_dryBasis_avg[3] = tmp;
  return 0;
}


///At boot, grab either the tmplog or the real log and count how many entries there are
///sprintf(sqlCmd,"select MAX(batch), count(*) from tblLog where batch = %d;", CalData.batch)
static int callbackBoot(void *pArg, int nArg, char **azArg, char **azCol)
{
  struct BOOT_DATA *p = (struct BOOT_DATA *)pArg;
  int tmp;
  
  if(azArg[0] == NULL) //Empty table, no data.
  {
    p->largestBatch = 0;
    p->count = 0;
    return 0;
  }
  
  sscanf(azArg[0],"%d",&tmp);
  p->largestBatch = tmp;
  sscanf(azArg[1],"%d",&tmp);
  p->count = tmp;
  
  return 0;
}



///After the first 30 or whatnot samples are taken, do some math to find what's typical.
///  sprintf(sqlCmd,"select AVG(field0_dryBasis), AVG(field1_dryBasis), AVG(field2_dryBasis), AVG(field3_dryBasis)  from tblTmpLog;");
static int callbackAvg(void *pArg, int nArg, char **azArg, char **azCol)
{
  float tmp;
  struct CAL_DATA *p = (struct CAL_DATA *)pArg;
  sscanf(azArg[0],"%f",&tmp);
  p->field_dryBasis_avg[0] = tmp;
  sscanf(azArg[1],"%f",&tmp);
  p->field_dryBasis_avg[1] = tmp;
  sscanf(azArg[2],"%f",&tmp);
  p->field_dryBasis_avg[2] = tmp;
  sscanf(azArg[3],"%f",&tmp);
  p->field_dryBasis_avg[3] = tmp;
  
  return 0;
}

///pArg points to a structure that'll be where I stick my end data
///nArg is # of columns in result
///azArg array of pointers to strings  with the data from  query
///azCol array of pointers to strings with the table names.
///  sprintf(sqlCmd,"select field0_dryBasis, field1_dryBasis, field2_dryBasis, field3_dryBasis  from tblTmpLog;"); 
static int callbackSD(void *pArg, int nArg, char **azArg, char **azCol)
{
  int rc;
  const char *zTable;
  const char *zType;
  const char *zSql;
  const char *zPrepStmt = 0;
  struct CAL_DATA *p = (struct CAL_DATA *)pArg;
  
  float tmp;
  sscanf(azArg[0],"%f",&tmp);  
  p->field_dryBasis_sd[0] +=  (tmp - p->field_dryBasis_avg[0]) * (tmp - p->field_dryBasis_avg[0]);
  sscanf(azArg[1],"%f",&tmp);
  p->field_dryBasis_sd[1] +=  (tmp - p->field_dryBasis_avg[1]) * (tmp - p->field_dryBasis_avg[1]);
  sscanf(azArg[2],"%f",&tmp);
  p->field_dryBasis_sd[2] +=  (tmp - p->field_dryBasis_avg[2]) * (tmp - p->field_dryBasis_avg[2]);
  sscanf(azArg[3],"%f",&tmp);
  p->field_dryBasis_sd[3] +=  (tmp - p->field_dryBasis_avg[3]) * (tmp - p->field_dryBasis_avg[3]);
  
  p->count++;
  
  return 0; //0 good, anything else, and it's GTFO
} 


///We should have the designated number of entries in that temporary table, so go through the db (for this batch) and calculate the average and standard deviation.
///Then clear that temporary table, apparently those samples are going to be rescanned.
void calcCalibration(int batch)
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  int i;
  
  if(logCount == 0)
  {
    printf("logCount is zero.\n");
    return;
  }
  
  sprintf(sqlCmd,"select AVG(field0_dryBasis), AVG(field1_dryBasis), AVG(field2_dryBasis), AVG(field3_dryBasis)  from tblTmpLog;");
  //printf("Running: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, callbackAvg, &CalData, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error fetching averages: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }   
  
  sprintf(sqlCmd,"select field0_dryBasis, field1_dryBasis, field2_dryBasis, field3_dryBasis  from tblTmpLog;"); 
  //printf("testing: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, callbackSD, &CalData, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error fetching CalData: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }      
  
  if(CalData.count == 0)
  {
    printf("we have a non-zero logCount, but nothing in the calData table?\n");
    return;
  }
  
  
  printf("\n\n-------------------------------\n");
  printf("Calibration pool size: %d\n",CalData.count);
  //Now div by count and take square root  to get the SD
  for(i=0; i < NUMFIELDS; i++)
  {
    CalData.field_dryBasis_sd[i] =  sqrt((CalData.field_dryBasis_sd[i])/ CalData.count);
    
    updateInterfaceFloat(&CalData.field_dryBasis_avg[i]);
    updateInterfaceFloat(&CalData.field_dryBasis_sd[i]);
    
    printf("%s AVG: %f\n", fieldNames[i], CalData.field_dryBasis_avg[i]);
    printf("%s SD: %f\n",  fieldNames[i], CalData.field_dryBasis_sd[i]);
  }
  printf("-------------------------------\n\n\n");
  
  //Sets up the initial rulsets based off of the SD values and pushes that to the GUI
  initRuleSet();
  
  //And then we need to clear out the temporary table.
  sprintf(sqlCmd,"DELETE FROM tblTmpLog;");
  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error while deleting tables: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }  
  
  return;
}



void makeReport()
{
  FILE* f;
  char filename[100];
  struct CAL_DATA ReportData;
  int i;
  
  sprintf(filename, "BatchReport_%d.txt",currentBatch);
  f = fopen(filename,"a");
  fprintf(f,"%s\n", LogData.headers[4]);
  fprintf(f,"%s\n", LogData.headers[0]);
  fprintf(f,"Sorted by: %s\n", LogData.headers[3]);
  fprintf(f,"Product test ID: %s\n", LogData.headers[5]);
  fprintf(f,"Strain name: %s\n", LogData.headers[6]);
  fprintf(f,"Comments: %s\n", LogData.headers[7]);
  fprintf(f,"Number of samples sorted: %d\n",logCount);
  
  fprintf(f,"\nAverages of the first %d samples:\n",calPoolSize); 
  fprintf(f,"\tAverage\tStandard Deviation\n");
  
  for(i=0; i < NUMFIELDS; i++)
  {
    fprintf(f,"%s: \t%5.2f\t%5.2f\n", fieldNames[i], CalData.field_dryBasis_avg[i] ,CalData.field_dryBasis_sd[i]);
  }
  
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;

  
  fprintf(f,"\nSorting results\n");
  for(i=0; i<NUMBINS; i++)
  {
    ReportData.count = 0;
    sprintf(sqlCmd,"select AVG(field0_dryBasis), AVG(field1_dryBasis), AVG(field2_dryBasis), AVG(field3_dryBasis), AVG(moisture_conc), count(*) from tblLog where bin=%d;",i);
    //printf("Running: [%s]\n",sqlCmd);
    rc = sqlite3_exec(db, sqlCmd, callbackReport, &ReportData, &zErrMsg);
    if( rc!=SQLITE_OK )
    {
      fprintf(stderr, "SQL error fetching averages: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      exit(-1);
    }
    fprintf(f,"Bin# %d\n",i);
    if(ReportData.count == 0)  //Issue with empty sets
    {
      fprintf(f,"\tCount: %d\n",0);
      fprintf(f,"\tmoisture_conc_avg: %f\n",0);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[0], 0);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[1], 0);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[2], 0);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[3], 0);  
    }
    else
    {
      printf("DEBUG: What the hell?: %f\n",ReportData.moisture_conc_avg);
      fprintf(f,"\tCount: %d\n",ReportData.count);
      fprintf(f,"\tmoisture_conc_avg: %f\n",ReportData.moisture_conc_avg);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[0], ReportData.field_dryBasis_avg[0]);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[1], ReportData.field_dryBasis_avg[1]);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[2], ReportData.field_dryBasis_avg[2]);
      fprintf(f,"\t%s_dryBasis_avg: %f\n",fieldNames[3], ReportData.field_dryBasis_avg[3]);
    }
  }  
  fprintf(f,"\n\n");
  
  
  fclose(f);
}


///Calibration has been done, now save that data to the db
void saveCalData()
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  
  sprintf(sqlCmd,"insert into tblCal values( %d, %d, %f, %f, %f, %f, %f, %f, %f, %f);",
    LogData.batch,
    CalData.count, 
    CalData.field_dryBasis_sd[0],
    CalData.field_dryBasis_avg[0],
    CalData.field_dryBasis_sd[1],
    CalData.field_dryBasis_avg[1],
    CalData.field_dryBasis_sd[2],
    CalData.field_dryBasis_avg[2],
    CalData.field_dryBasis_sd[3],
    CalData.field_dryBasis_avg[3] );

  //printf("testing: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error saving cal data: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }      
  return;
}

///Clear out the data so we know we're pre-calibration
///So.... we could store this data, and increment the batchcount to differentiate... But then we'd need a way to know if we're pre- or post- calibration, which is why I'm making this in the first place...
void clearCalData()
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  
  sprintf(sqlCmd,"delete FROM tblCal;");  //Yeah, that's everything
  //printf("testing: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error clearing data: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }      
  return;
}

///Insert LogData into db.  if permFlag, then it gets logged into the temporary table which gets wipped after we get calcSD/Avg when there are 30 entries 
void insertLog(int permFlag)
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  
  char table[TYPICALSIZE];
  
  if(permFlag)
    sprintf(table, "tblLog");
  else
    sprintf(table, "tblTmpLog");
  
  sprintf(sqlCmd,"insert into %s values( %d,\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %d);",
    table,
    LogData.batch,
    LogData.headers[0],
    LogData.headers[1],
    LogData.headers[2],
    LogData.headers[3],
    LogData.headers[4],
    LogData.headers[5],
    LogData.headers[6],
    LogData.headers[7],

    LogData.field_conc[0],
    LogData.field_mDist[0],
    LogData.field_dryBasis[0],
    LogData.field_conc[1],
    LogData.field_mDist[1],
    LogData.field_dryBasis[1],
    LogData.field_conc[2],
    LogData.field_mDist[2],
    LogData.field_dryBasis[2],
    LogData.field_conc[3],
    LogData.field_mDist[3],
    LogData.field_dryBasis[3],

    LogData.moisture_conc,
    LogData.moisture_mDist,
    LogData.bin);

  //printf("testing: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error inserting new log: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }      
  return;
}



///Dan wants the ability to reject the last sample. This is called in the button callback, and should only happen when it was a valid sample.
void lof_reject()
{
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char table[20];
  char *zErrMsg = 0; 
  
  if(calFlag)
    sprintf(table, "tblLog");
  else
    sprintf(table, "tblTmpLog");
  
  sprintf(sqlCmd,"DELETE FROM %s where rowid in (select max(rowid) from tblTmpLog);", table);
  rc = sqlite3_exec(db, sqlCmd, NULL, 0, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    dualPrintf("SQL ERR");
    fprintf(stderr, "SQL error while rejecting last file: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
  }  
  

}

void lof_exit()
{
  sqlite3_close(db);
}


/**We analyse database and see where we're starting. What's the latest batch. Pick up calibration and shit
1) Make calData log in SQL so we retain the critical knowledge needed for a batch.
    -Save data into it when we hit cal
    -clear it at the start of a new batch, 
2) Check calData log. If it exists, we're past cal and calFlag is 1. Load that data.
3nocal) If there's any data in tblTmpLog, count the number in there, get latest batch recorded, and count
3cal) Look at tblLog. Get latest batch, count
*/
void wsql_init()
{
  //TODO move to it's own func
  int rc;
  char sqlCmd[1000]; //Still not giving a shit
  char *zErrMsg = 0;
  struct BOOT_DATA BootData;
  int i;

  //If it doesn't already exist, we'll need to make the tables
  int alreadyThere = fileExists("db.sql");
  
  //open / make database
  sqlite3_open( "db.sql", &db);
  if(!alreadyThere)
  {
    printf("first time? making tables\n");
    makeTables();
  }
  //ELSE TODO: Some sanity checking on the integrity and format of that database. 
  

  
  sprintf(sqlCmd,"select * from tblCal;");
  //printf("Running: [%s]\n",sqlCmd);
  rc = sqlite3_exec(db, sqlCmd, callbackCalData, &CalData, &zErrMsg);
  if( rc!=SQLITE_OK )
  {
    fprintf(stderr, "SQL error fetching averages: %s\n", zErrMsg);
    sqlite3_free(zErrMsg);
    exit(-1);
  }
  if(CalData.count == 0) // no cal log
  {
    //count how many logs are in tblTmpLog, get the latest batch recorded (they "should" all agree)
    //Set batch and count
    sprintf(sqlCmd,"select MAX(batch), count(*) from tblTmpLog;");
    //printf("NoCal, Running: [%s]\n",sqlCmd);
    rc = sqlite3_exec(db, sqlCmd, callbackBoot, &BootData, &zErrMsg);
    if( rc!=SQLITE_OK )
    {
      fprintf(stderr, "SQL error fetching averages: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      exit(-1);
    }
    calFlag = 0;
    logCount = BootData.count;
    currentBatch = BootData.largestBatch;

  }
  else
  {
    //calData exists, post to GUI, set calFlag to 1, set current batch to that number, count number in tblLog 
    sprintf(sqlCmd,"select MAX(batch), count(*) from tblLog where batch = %d;", CalData.batch);
    //printf("Got Cal, Running: [%s]\n",sqlCmd);
    rc = sqlite3_exec(db, sqlCmd, callbackBoot, &BootData, &zErrMsg);
    if( rc!=SQLITE_OK )
    {
      fprintf(stderr, "SQL error fetching averages: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
      exit(-1);
    }
    calFlag = 1;
    calPoolSize = CalData.count;
    logCount = BootData.count;
    currentBatch = CalData.batch;
    
    
    for(i=0; i < NUMFIELDS; i++)
    {
      updateInterfaceFloat(&CalData.field_dryBasis_avg[i]);
      updateInterfaceFloat(&CalData.field_dryBasis_sd[i]);
    }
    initRuleSet();
    
  }
  updateInterfaceInt(&currentBatch);
  updateInterfaceInt(&logCount);  
  updateInterfaceInt(&calPoolSize);
  updateInterfaceFloat(&mDistThreshold);
  //TODO figure out a way to get unique batch number across multiple devices so we could consolidate all the databases into one company-wide database.
  
  //Buuuuuuuut Dan doesn't give a shit about that long-term data stuff, so fuck it.
}