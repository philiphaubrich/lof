#ifndef _wsql_H_
#define _wsql_H_

#include "sqlite3.h"

void wsql_init();

void lof_exit();  
void lof_reject();

void calcCalibration(int batch);
void saveCalData();
void clearCalData();
void insertLog(int permFlag);

#endif
