#ifndef _RULESET_H_
#define _RULESET_H_


#define NUMDROP 23

struct RULE_SET 
{
  int targetBin;  // 1-9
  float* field; //crazy-ass pointer shenanigians, but it's refreshingly direct
  int comparator; //  > < = !=
  float value;
} ;


void clearRuleSet();
int lookupField(float* fltPtr);
void initRuleSet();
void binSort();

extern float* FieldLookup[NUMDROP];
extern struct RULE_SET RuleSet[];

#endif
