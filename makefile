all: gui.c gui.h lof.h lof.o sqlite3.o wsql.o ruleSet.o
	gcc gui.c ruleSet.o wsql.o lof.o sqlite3.o -I c:\MinGW\include\gtk-2.0 -I c:\MinGW\include\glib-2.0  -I c:\MinGW\lib\glib-2.0\include  -I c:\MinGW\lib\gtk-2.0\include -I c:\MinGW\include\cairo   -I c:\MinGW\include\pango-1.0   -I c:\MinGW\include  -I c:\MinGW\include\atk-1.0  -mms-bitfields -L c:\MinGW\bin -lgtk-win32-2.0 -lglib-2.0 -lgobject-2.0 -o forLoveOfFamily_v3.exe -g

lof.o: lof.c gui.h wsql.o ruleset.o
	gcc lof.c -c -g
  
ruleSet.o: ruleSet.c ruleSet.h gui.h lof.h
	gcc ruleSet.c -c -g
  
wsql.o: wsql.c sqlite3.c lof.h
	gcc wsql.c sqlite3.c -DSQLITE_THREADSAFE=0 -DSQLITE_OMIT_LOAD_EXTENSION -c -g
#-mwindows

#sudo apt-get install libgtk-3-dev
linux: gui.c gui.h lof.h lof.o sqlite3.o wsql.o ruleSet.o
	gcc gui.c ruleSet.o wsql.o lof.o sqlite3.o -o forLoveOfFamily_v3 -g  `pkg-config --cflags --libs gtk+-3.0`

  
clean: 
	rm *.o
